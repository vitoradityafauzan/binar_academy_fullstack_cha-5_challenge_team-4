const express = require("express");
const axios = require("axios");
const multer = require("multer");
const bodyParser = require("body-parser");
const fs = require("fs");
const path = require("path");
var methodOverride = require("method-override");
const {
  Op
} = require("sequelize");

const app = express();
const {
  Car,
  CarType
} = require("./models");
const {
  response
} = require("express");
const moment = require("moment");
const PORT = process.env.PORT || 7000;

// Untuk mememasukan gambar
const diskStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, "./static/upload")); // set the destination
  },
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    ); // set the file name dynamically
  },
});

// EJS
app.use(methodOverride("_method"));
app.set("view engine", "ejs");
app.use("/static", express.static("static"));
app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.get("/", (req, res) => {
  axios
    .get("http://localhost:7000/api/v1/cars")
    .then((response) => {
      res.render("pages/cars", {
        cars: response.data,
        moment: moment
      });
    })
    .catch((error) => {
      console.log(error);
    });
});

app.get("/addCars", (req, res) => {
  try {
    res.render("pages/addCars");
  } catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
});

app.get("/editCars/:id", (req, res) => {
  axios
    .get("http://localhost:7000/api/v1/cars/" + req.params.id)
    .then((response) => {
      res.render("pages/editCars", {
        cars: response.data
      });
    })
    .catch((error) => {
      console.log(error);
    });
});
// EJS

// API
// Get All Car And TypeCar
app.get("/api/v1/cars", async (req, res) => {
  try {
    cars = await Car.findAll({
      include: [{
        model: CarType,
        as: "size",
      }, ],
    });
    res.status(200).json(cars);
  } catch (error) {
    res.status(500).json({
      error: error.message,
    });
  }
});

// Create Car
app.post(
  "/api/v1/cars",
  multer({
    storage: diskStorage,
  }).single("image_car"),
  async (req, res, next) => {
    const cars = new Car({
      name: req.body.name,
      price: req.body.price,
      photo: req.file.filename,
      typeId: req.body.typeId,
    });
    cars.save().then((result) => {
      res.send(
        '<script>alert("Data Mobil Berhasil Di Simpan!");window.location.href="/";</script>'
      );
    });
  }
);

// Type Car
app.post("/api/v1/CarType", async (req, res) => {
  CarType.create(req.body)
    .then((CarType) => {
      res.status(201).json(CarType);
    })
    .catch((error) => {
      res.status(500).json({
        error: error.message,
      });
    });
});

// Update Cars
app.get("/api/v1/cars/:id", async (req, res) => {
  try {
    const cars = await Car.findOne({
      where: {
        id: req.params.id,
      },
      include: [{
        model: CarType,
        as: "size",
      }, ],
    });
    res.status(200).json(cars);
  } catch (error) {
    res.status(500).json({
      error: error.message,
    });
  }
});

app.put("/api/v1/cars/:id", multer({
    storage: diskStorage,
  }).single("image_car"),
  async (req, res) => {
    try {
      const cars = await Car.findOne({
        where: {
          id: req.params.id,
        },
      });
      if (req.file) {
        fs.unlinkSync(path.join(__dirname, "./static/upload/" + cars.photo), (err) => {
          if (err) throw err;
        });
        cars.update({
          name: req.body.name,
          price: req.body.price,
          photo: req.file.filename,
          typeId: req.body.typeId,
        });
      } else {
        cars.update({
          name: req.body.name,
          price: req.body.price,
          typeId: req.body.typeId,
        });
      }
      res.send(
        '<script>alert("Data Mobil Berhasil Di Update!");window.location.href="/";</script>'
      );
    } catch (error) {
      res.status(500).json({
        error: error.message,
      });
    }
  }
);

// Delete Baru
app.delete("/deleteCars/:id", async (req, res) => {
  try {
    const cars = await Car.findOne({
      where: {
        id: req.params.id,
      },
    });
    fs.unlinkSync(path.join(__dirname, "./static/upload/" + cars.photo));
    await cars.destroy();
    res.redirect("/");
  } catch (error) {
    res.status(500).json({
      error: error.message,
    });
  }
});

// Search
app.get("/api/v1/cars/search/:name", async (req, res) => {
  try {
    const cars = await Car.findAll({
      where: {
        name: {
          [Op.like]: "%" + req.params.name + "%",
        },
      },
      include: [{
        model: CarType,
        as: "size",
      }, ],
    });
    res.status(200).json(cars);
  } catch (error) {
    res.status(500).json({
      error: error.message,
    });
  }
});

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));