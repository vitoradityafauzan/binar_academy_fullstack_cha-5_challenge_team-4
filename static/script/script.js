function insertChildLink(link) {
  const lower = `${link}`.toLowerCase();
  const ref = lower.replace(/\s/g, "");
  // console.log(ref)
  return `<a class="child-link" href="${ref}.html">${link}</a> <br><br>`;
}

function sideNav(linkParent, linkOne) {
  const parentLink = document.querySelector(".parent-link");
  parentLink.innerHTML = linkParent;

  const childLink = document.querySelector(".child");
  childLink.innerHTML = insertChildLink(linkOne);

  const sideNav = document.getElementById("mySidenav");
  const main = document.getElementById("main");
  console.log(linkParent);
  if (sideNav.style.width == 0 || sideNav.style.width == "0px") {
    sideNav.style.width = "160px";
    sideNav.style.marginLeft = "70px";
    main.style.marginLeft = "230px";
  }
}

function humberger(currentLink, listLink) {
  const parentLink = document.querySelector(".parent-link");
  parentLink.innerHTML = currentLink;

  const childLink = document.querySelector(".child");
  childLink.innerHTML = insertChildLink(listLink);

  const sideNav = document.getElementById("mySidenav");
  const main = document.getElementById("main");
  if (sideNav.style.width == 0 || sideNav.style.width == "0px") {
    sideNav.style.width = "160px";
    sideNav.style.marginLeft = "70px";
    main.style.marginLeft = "230px";
  } else {
    sideNav.style.width = 0;
    sideNav.style.marginLeft = "0";
    main.style.marginLeft = "70px";
  }
  console.log(sideNav.style.width);
}
