# Rental Cars Management Site

Made for Chapter 5 Group Assignment
Binar KM SIB#2 - FSW 4

 <br><br><br>
#### By - Team 5
------
1. Andien Khansa'A Iffat Paramarta
1.  Rayhan Yuda Lesmana
1.  Salma Eka Yudanti
1.  Vito Raditya Fauzan

<br><br><br>

#### Tools / Modules Used
------
- WSL
- Visual Studio Code
- PostgreSQL
- Sequelize
- Express JS
- Axios
- Multipart
- BodyParser
- File System (FS)
- Path
- Method-Override

<br><br><br>

#### Entity Relational Diagram
------
![image](https://drive.google.com/uc?export=view&id=1aDrgoS8pgKH5sHhbgml-TfGm0tN6jvyP)

<br> <br><br>

#### Web Content
------
##### Home Page (By Rayhan Yuda Lesmana)
![image](https://drive.google.com/uc?export=view&id=18S5IKODmLZ25RMXHVToAsi1OTcZSnX_8)
<br><br>
##### Add New Car Page (By Vito Raditya Fauzan)
![image](https://drive.google.com/uc?export=view&id=1-_tK_p_TbAZrH0JuZ6B_5lxsvegrXhyN)
<br><br>
##### Update Car Page (By Salma Eka Yudanti)
![image](https://drive.google.com/uc?export=view&id=1K20Dj-J3rNz3O9j4rWXbT_LhvbuugYl3)
<br><br>
##### Delete Car Page (By Andien Khansa'A Iffat Paramarta)
![image](https://drive.google.com/uc?export=view&id=1MrABcVXvonGGN66dmRGLlyirfwTVW7Op)
